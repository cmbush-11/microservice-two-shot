# Generated by Django 4.0.3 on 2023-07-20 00:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_alter_hat_location'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hat',
            name='name',
        ),
    ]
