import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import CreateHatForm from './CreateHatForm';
import ShoesList from './ShoesList'
import CreateShoeForm from './CreateShoeForm';

function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">

        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/" element={<HatsList hats={props.hats} />} />
          <Route path="shoes/" element={<ShoesList shoes={props.shoes} />} />
          <Route path="createHatForm/" element={<CreateHatForm />} />
          <Route path="createShoeForm/" element={<CreateShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
