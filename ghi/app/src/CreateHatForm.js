import React, { useEffect, useState } from 'react';

function CreateHatForm(props) {

    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);
    const [isSubmitted, setIsSubmitted] = useState(false);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;
        
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const hatResponse = await fetch(hatUrl, fetchOptions);
        if (hatResponse.ok) {
            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
            setIsSubmitted(true);
            props.getHats();
        }
    }

    const handleChangeFabric = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleChangeStyleName = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleChangeLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    let dropdownClasses = 'form-select';
    let messageClasses = 'alert alert-success d-none mb-0';


    return (
        <div className='my-5 container'>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <div className='shadow p-4 mt-4'>
                        <h1>Create a Hat!</h1>
                        <p>Tell us about your hat!</p>
                        <form onSubmit={handleSubmit}>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeStyleName} required placeholder='Hat Style Name' type='text' id='styleName' name='styleName' className='form-control' />
                            <label htmlFor='styleName'>Style Name</label>
                        </div>
                        <div className='col'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeColor} required placeholder='Hat Color' type='text' id='color' name='color' className='form-control' />
                            <label htmlFor='color'>Color</label>
                        </div>
                        </div>
                    < div className='col'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeFabric} required placeholder='Hat Fabric' type='text' id='fabric' name='fabric' className='form-control' />
                            <label htmlFor='fabric'>Fabric</label>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangePictureUrl} required placeholder='Picture URL' type='text' id='pictureUrl' name='pictureUrl' className='form-control' />
                            <label htmlFor='pictureUrl'>Picture Url</label>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='mb-3'>
                            <select onChange={handleChangeLocation} value ={location} name='location' id='location' className={dropdownClasses} required>
                                <option value=''>Choose a Location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                                    )
                                })}
                            </select> 
                        </div>
                    </div>
                        <button className='btn btn-lg btn-primary'>Create a Hat!</button>
                        { isSubmitted === true && ( 
                            <div className='alert alert-success mb-0' id='success-message'>
                                <p></p>
                                <p>Congratulations! You created a hat!</p>
                            </div>
                        )}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreateHatForm;
