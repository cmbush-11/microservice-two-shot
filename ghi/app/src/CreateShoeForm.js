import React, { useEffect, useState } from 'react';

function CreateShoeForm(props) {

    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureURL, setPictureURL] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);
    const [isSubmitted, setIsSubmitted] = useState(false);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_URL = pictureURL;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {
            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureURL('');
            setBin('');
            setIsSubmitted(true);
            props.getShoes();
        }
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangePictureURL = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    }

    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    let dropdownClasses = 'form-select';
    let messageClasses = 'alert alert-success d-none mb-0';


    return (
        <div className='my-5 container'>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <div className='shadow p-4 mt-4'>
                        <h1>Create a Shoe</h1>
                        <p>Shoe Details</p>
                        <form onSubmit={handleSubmit}>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeManufacturer} required placeholder='Shoe Manufacturer' type='text' id='Manufacturer' name='Manufacturer' className='form-control' />
                            <label htmlFor='manufacturer'>Manufacturer</label>
                        </div>
                        <div className='col'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeModelName} required placeholder='Shoe Model' type='text' id='modelName' name='modelName' className='form-control' />
                            <label htmlFor='modelName'>Shoe Model</label>
                        </div>
                        </div>
                    < div className='col'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangeColor} required placeholder='Color' type='text' id='color' name='color' className='form-control' />
                            <label htmlFor='color'>Color</label>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleChangePictureURL} required placeholder='Picture URL' type='text' id='pictureURL' name='pictureURL' className='form-control' />
                            <label htmlFor='pictureURL'>Picture URL</label>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='mb-3'>
                            <select onChange={handleChangeBin} value ={bin} name='bin' id='bin' className={dropdownClasses} required>
                                <option value=''>Choose a Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                    </div>
                        <button className='btn btn-lg center btn-primary'>Create a Shoe</button>
                        { isSubmitted === true && (
                            <div className={messageClasses} id='success-message'>
                                <p></p>
                                <p>Shoe Created</p>
                            </div>
                        )}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreateShoeForm;
