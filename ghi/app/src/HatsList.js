function HatsList(props) {
    const deleteHat = async(hat) => {
        try {
            const deleted = hat.id
            const hatUrl = `http://localhost:8090/api/hats/${deleted}/`;
            const fetchConfig = {
                method: "delete",
            };
            const response = await fetch(hatUrl, fetchConfig);
            if (response.ok) {
                console.log("Hat deleted");
                window.location.reload(false);
            };
        }
        catch (e) {
            console.log(e)
        }
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (<tr key={hat.id}>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.picture_url }</td>
                        <td>{ hat.location }</td>
                        <td><button onClick={() => deleteHat(hat)}>Delete</button></td>
                    </tr>)
                })}
            </tbody>
        </table>
    )
}

export default HatsList;