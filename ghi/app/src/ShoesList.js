function ShoesList(props) {
    const deleteShoe = async(shoe) => {
        try {
            const deleted = shoe.id
            const shoeURL = `http://localhost:8080/api/shoes/${deleted}/`;
            const fetchConfig = {
                method: "delete",
            };
            const response = await fetch(shoeURL, fetchConfig);
            if (response.ok) {
                console.log("Shoe Deleted");
                window.location.reload(false);
            };
        }
        catch (e) {
            console.log(e)
        }
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (<tr key={shoe.id}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td>{ shoe.picture_url }</td>
                        <td>{ shoe.bin }</td>
                        <td><button onClick={() => deleteShoe(shoe)}>Delete</button></td>
                    </tr>)
                })}
            </tbody>
        </table>
    )
}

export default ShoesList;
