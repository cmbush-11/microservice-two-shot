# Wardrobify

Team:

* Person 1 - Christopher Bush - Shoes
* Person 2 - Miguel Ortiz - Hats

## Design

## Shoes microservice

I created a model for shoe and a VO of the preexisting bin model. Created views to show shoes and lists of shoes. Used a poller to incorporate this info into my microservice and used JSON and react to show it visually.


## Hats microservice

I created a Value Object of the Location Object so that I could use the Location information from the wardrobe api microservice and display it in my necessary views. I brought that information to my microservice using a poller, passed it as JSON to the front end using my views and then displayed it using my react code.
